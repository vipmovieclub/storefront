FROM python:3

EXPOSE 5000

RUN mkdir /storefront
WORKDIR /storefront

COPY requirements.txt /storefront/requirements.txt
RUN pip install -r requirements.txt

COPY . /storefront

CMD ["python", "apprunner.py"]