from flask_paginate import Pagination

def get_css_framework(current_app):
    return current_app.config.get('CSS_FRAMEWORK', 'bootstrap4')


def get_link_size(current_app):
    return current_app.config.get('LINK_SIZE', 'sm')


def get_alignment(current_app):
    return current_app.config.get('LINK_ALIGNMENT', '')


def show_single_page_or_not(current_app):
    return current_app.config.get('SHOW_SINGLE_PAGE', False)


def get_pagination(current_app, **kwargs):
    kwargs.setdefault('record_name', 'records')
    return Pagination(css_framework=get_css_framework(current_app),
                      link_size=get_link_size(current_app),
                      alignment=get_alignment(current_app),
                      show_single_page=show_single_page_or_not(current_app),
                      **kwargs)