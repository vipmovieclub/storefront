from flask_login import LoginManager

# Create a LoginManger object that can be imported to other classes
login_manager = LoginManager()

# set the login view that a 405 (Unauthorised) is redirected to
login_manager.login_view = 'login.login_page'