from flask_nav import Nav
from flask_nav.elements import *
from flask_login import current_user

nav = Nav()


def top_nav():
    """:return a Navbar object that contains the view items
    This method is called every time a page is loaded so we can do dynamic construction,
    for example, to display logged in users"""

    items = [View('VIP Movie Club', 'landing.index_page'),
             View('Home', "items.item_listing")]

    # show a different nav bar if the user is logged in or not
    if current_user.is_authenticated is False:
        items.append(View('Login', 'login.login_page'))
        items.append(View('Register', 'login.register'))
    else:
        items.append(Text('Welcome {}!'.format(current_user.first_name)))
        items.append(View('My Profile', 'profile.my_profile'))
        items.append(View('My Cart', 'cart.cart_listing'))
        items.append(View('Logout', 'login.logout'))

    return Navbar('', *items)
