from flask import Blueprint, render_template, current_app, request
from flask_login import login_required
import requests
from flask_paginate import get_page_args
import util.pagination as paginate_functions

# Create the blueprint variable for routes and to register in the app factory
items = Blueprint('items', __name__)


@items.route('/items/')
@login_required
def item_listing():
    """:return The item page showing all search items"""

    # page is the variable assigned to getting the current page, eg '1'
    # per_page is configured in the util/pagination.cfg, represents the amount of items per page to be displayed
    # offset is a calculated value to use when querying the db for the next page's items to display
    page, per_page, offset = get_page_args(page_parameter='page', per_page_parameter='per_page')

    # search for all listings using a wildcard in the query (implemented in inventory_search)
    query_name = 'All'

    # send request to inventory_search to get all listings
    r = requests.post('http://inventory_search:5002/allListings',
                      data={'per_page': per_page, 'offset': offset})

    # Get count of items for pagination 'total' variable
    r_count = requests.get('http://inventory_search:5002/count_allListings')
    count = r_count.json()[0]['count']

    # set up a pagination object
    pagination = paginate_functions.get_pagination(current_app, page=page,
                                per_page=per_page,
                                total=count,
                                record_name='listings',
                                format_total = True,
                                format_number = True,)

    # return the page to be displayed with a pagination object
    return render_template("items/store_items.html",
                           items=r.json(),
                           query_name=query_name,
                           page=page,
                           count=count,
                           per_page=per_page,
                           pagination=pagination,
                           )


@items.route('/items/search/', defaults={'page': 1})
@items.route('/items/search/page/<int:page>/')
@login_required
def search(page):
    """:return a page giving the items relating to a search"""

    # page is the variable assigned to getting the current page, eg '1'
    # per_page is configured in the util/pagination.cfg, represents the amount of items per page to be displayed
    # offset is a calculated value to use when querying the db for the next page's items to display
    page, per_page, offset = get_page_args(page_parameter='page', per_page_parameter='per_page')


    # get args and set query arguments to search the inventory_db
    search_string = request.args.get('search')
    query_name = search_string

    # send request to inventory_search to search for search_query
    r = requests.post('http://inventory_search:5002/Search',
                      data={'per_page': per_page, 'offset': offset, 'search_string': search_string})

    # Get count of items for pagination 'total' variable
    r_count = requests.post('http://inventory_search:5002/count_Search',
                      data={'search_string': search_string})
    count = r_count.json()[0]['count']

    # set up a pagination object
    pagination = paginate_functions.get_pagination(current_app, page=page,
                                                   per_page=per_page,
                                                   total=count,
                                                   record_name='listings',
                                                   format_total=True,
                                                   format_number=True, )

    # return the page to be displayed with a pagination object
    return render_template("items/store_items.html",
                           items=r.json(),
                           query_name=query_name,
                           count=count,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           )
