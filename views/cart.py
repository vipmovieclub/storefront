from flask import Flask, Blueprint, render_template, current_app, request, redirect, url_for
from flask_login import current_user, login_required
from model import User, db
import requests

# Create the blueprint variable for routes and to register in the app factory
cart = Blueprint('cart', __name__)


@cart.route('/addToCart', methods=['POST'])
@login_required
def add_to_cart():
    # get the itemID from the POST request
    itemID = request.form['itemID']
    itemName = request.form['itemName']
    itemPrice = request.form['itemPrice']

    # get the userID of the current session

    userID = current_user.id

    # send a request to the cart and purchasing db to add the item to the cart of the current user
    requests.post('http://cart_purchasing:5004/add_to_cart',
                      data={'userID': userID, 'itemID': itemID, 'itemPrice': itemPrice, 'itemName': itemName})

    return redirect(url_for('items.item_listing'))


@cart.route('/cart')
@login_required
def cart_listing():

    # get the userID of the current session
    userID = current_user.id

    # get the cart for the current_user
    r = requests.post('http://cart_purchasing:5004/cart_list', data={'userID': userID})

    # calculate subtotal
    subtotal = 0
    for item in r.json():
        subtotal += float(item['itemPrice'])

    # return the page to be displayed for a cart
    return render_template('cart/cart.html', cart=r.json(), subtotal=subtotal)


@cart.route('/delete_from_cart', methods=['POST'])
@login_required
def cart_delete_item():

    # get the userID of the current session
    userID = current_user.id

    item_to_delete = request.form['delete']

    # get the cart for the current_user
    requests.post('http://cart_purchasing:5004/delete_item', data={'userID': userID, 'itemID': item_to_delete})

    # return the page to be displayed for a cart
    return redirect(url_for('cart.cart_listing'))
