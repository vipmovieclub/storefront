from flask import Blueprint, render_template

# Create the blueprint variable for routes and to register in the app factory
landing = Blueprint('landing', __name__)


@landing.route('/')
def index_page():
    """:return The index/landing page"""
    return render_template("landing.html")
