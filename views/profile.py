from flask import Blueprint, render_template
from flask_login import login_required

# Create the blueprint variable for routes and to register in the app factory
profile = Blueprint('profile', __name__)


@profile.route('/myProfile')
@login_required
def my_profile():
    """:return The profile page for the current user"""
    return render_template("profile/profile.html")
