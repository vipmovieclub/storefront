from flask import Blueprint, render_template, redirect, request, flash, url_for
from flask_login import login_user, logout_user, current_user
from model import User, db
import requests

# Create the blueprint variable for routes and to register in the app factory
login = Blueprint('login', __name__)


@login.route('/login/')
def login_page():
    """:return A login page"""
    return render_template("login/login.html")

@login.route('/login/', methods=['POST'])
def login_form():
    """Form handler for login page"""
    # get data from the login form
    email = request.form['email']
    password = request.form['password']

    # get a corresponding user object if the email exists in the user_db
    user = User.query.filter_by(email=email).first()

    # check if the user does not exist or if the password provided does not match the user_db
    if user is None or not user.check_password(password):
        flash('Invalid username or password, please try again.')
        return redirect(url_for('login.login_page'))

    # login the user
    login_user(user)

    # handle the redirect of the page
    next_page = request.args.get('next')
    if not next_page:
        next_page = url_for('items.item_listing')
    return redirect(next_page)


@login.route('/register/')
def register():
    """:return A registration page"""

    ## Create a test user when the register page is visited ##
    if User.query.filter_by(email='test@test.com').first() is None:
        test_user = User(first_name='Test', last_name='User', email='test@test.com')
        test_user.set_password('test')
        db.session.add(test_user)
        db.session.commit()

    return render_template("login/register.html")


@login.route('/register/', methods=['POST'])
def create_account():
    """Post method to create an account in the app
    """
    # get the data from the registration form
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    email = request.form['email']
    password = request.form['password']

    # create a new user object with sqlalchemy, set the password and commit to the db
    user = User(first_name=first_name, last_name=last_name, email=email)
    user.set_password(password)
    db.session.add(user)
    db.session.commit()

    return redirect(url_for("login.login_page"))

@login.route('/logout/')
def logout():
    """Logout a user from the current session"""
    userID = current_user.id
    requests.post('http://cart_purchasing:5004/delete_cart', data={'userID': userID})
    logout_user()

    return redirect(url_for('landing.index_page'))