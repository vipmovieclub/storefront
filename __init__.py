from flask import Flask
from flask_bootstrap import Bootstrap

# Area to import the various blueprints
from views.login import login
from views.items import items
from views.landing import landing
from views.cart import cart
from views.profile import profile


def create_app():
    """Creates the app that can be within the builtin flask webserver. To run execute apprunner.py
    :return Flask application
    TODO: change to run on AWS ElasticBeanstalk/AWS ECS or similar after development"""

    # Create the Flask app, following the applicationFactory pattern
    app = Flask(__name__)

    # add support for user management with SQLAlchemy
    from model import db
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@user_db/users'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.secret_key = 'VIPMovieclub'
    db.init_app(app)

    # add login and session support with flask-login
    from util.login import login_manager
    login_manager.init_app(app)

    # load config for pagination
    app.config.from_pyfile('util/pagination.cfg')

    # Register the Bootstrap extension
    Bootstrap(app)

    # Register the Flask-Nav navigation bar
    from util.navigation_bar import nav, top_nav
    nav.init_app(app)
    nav.register_element('top_nav', top_nav)

    # Area to register various blueprints for Storefront sections
    app.register_blueprint(login, url_prefix='/tenant1')
    app.register_blueprint(items, url_prefix='/tenant1')
    app.register_blueprint(landing, url_prefix='/tenant1')
    app.register_blueprint(cart, url_prefix='/tenant1')
    app.register_blueprint(profile, url_prefix='/tenant1')

    # return the initialised app
    return app
